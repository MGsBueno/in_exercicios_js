while(true){
    
    var number_one = prompt ("digite um numero")
    if(number_one == "sair"){
        alert("Você saiu")
        break
    }
    var number_two = prompt("digite outro número")
    if(number_two == "sair"){
        alert("Você saiu")
        break
    }
    var operator = prompt("digite uma das opções:\n0 - adição\n1 - subtração\n2 - multiplicação\n3 - divisão\n4 - potência\n5 - alterar numeros inseridos")
    if(operator == "sair"){
        console.log("Você saiu")
        break
    }
    
    //utlizando switch case para melhorar a legibilidade 
    switch(operator){
        case "0":
        alert(
            number_one + " + " + number_two + " = " +
                    (parseInt(number_one)+parseInt(number_two)
                    )
            );
            break;

        case "1":
            alert(number_one + " - " + number_two + " = " +
                    (parseInt(number_one)-parseInt(number_two)
                    )
                );
            break;  
            
        case "2":
            alert(number_one + " * " + number_two + " = " +
                    (parseInt(number_one)*parseInt(number_two)
                    )
            );
            break;  

        case "3":
            alert(number_one + " / " + number_two + " = " +
                    (parseInt(number_one)/parseInt(number_two)
                    )
            );
            break;

        case "4":
            alert(number_one + " ^ " + number_two + " = " +
                    (parseInt(number_one)**parseInt(number_two)
                    )
            );
            break;

        case "5":
            break;
        default:
            alert("operação inválida!");
            break;   
        
    }
}
   